import ThreeMaze from './maze.js'
import Alpine from 'alpinejs'
import { SERVER_URL } from './params.js'

var maze = undefined;
var chart = undefined;
const wrapper = document.querySelector('.three');

const load = () => {

    let id = Alpine.store('gameId');

    fetch(`${SERVER_URL}/game?id=${id}`)
        .then(res => {

            // check for error response
            if (!res.ok) {
                // get error message from body or default to response status
                const error = response.status;
                return Promise.reject(error);
            }

            // remove info message (if exists)
            Alpine.store('gameInfo', "");

            return res.json();
        })
        .then(data => {
            // initialize game
            wrapper.innerHTML = ""
            Alpine.store('gameEnd', false);
            Alpine.store('gameRound', data["round"]);
            Alpine.store('gameMaxRounds', data["maxRounds"]);
            Alpine.store('gameHider', data["hider"]);
            Alpine.store('gameSeeker', data["seeker"]);

            let players = []
            let hider_loacations = [];
            let hider_names = [];
            let seeker_loacations = [];
            let seeker_names = [];
            let average_distance = 0;

            // initialize players data for statistics
            for (let i = 0; i < data["players"].length; i++) {
                let player_info = []
                player_info["steps"] = 0
                player_info["name"] = data["players"][i]["name"]
                if (data["players"][i]["roleStr"] === "HIDER") {
                    player_info["role"] = "Hider"
                    player_info["type"] = data["hider"]
                } else if (data["players"][i]["roleStr"] === "SEEKER") {
                    player_info["role"] = "Seeker"
                    player_info["type"] = data["seeker"]
                }
                players.push(player_info);
                // for distance calculation
                if (data['players'][i]['roleStr'] === "SEEKER") {
                    seeker_loacations.push(data['players'][i]['location']);
                    seeker_names.push(data['players'][i]['name']);
                } else if (data['players'][i]['roleStr'] === "HIDER") {
                    hider_loacations.push(data['players'][i]['location']);
                    hider_names.push(data['players'][i]['name']);
                }
            }

            // store players data for statistics
            Alpine.store('players', players)
            Alpine.store('found', 0);
            Alpine.store('changedBlocks', 0);

            let hiders_seekers_distances = {}

            // caluculate distances
            for (let j = 0; j < seeker_loacations.length; j++) {
                hiders_seekers_distances[seeker_names[j]] = {}
                for (let k = 0; k < hider_loacations.length; k++) {
                    hiders_seekers_distances[seeker_names[j]][hider_names[k]] = [];
                    let distance = Math.sqrt(Math.pow(hider_loacations[k]['x'] - seeker_loacations[j]['x'], 2) +
                        Math.pow(hider_loacations[k]['y'] - seeker_loacations[j]['y'], 2));
                    average_distance += distance;
                    hiders_seekers_distances[seeker_names[j]][hider_names[k]].push({ y: distance });
                }
            }
            average_distance /= hider_loacations.length * seeker_loacations.length;

            let average = [];

            average.push({ y: average_distance });

            // store chart data
            Alpine.store("average", average);
            Alpine.store("hiders_seekers_distances", hiders_seekers_distances);

            let data_chart = [{
                name: 'Average',
                type: 'spline',
                showInLegend: true,
                dataPoints: Alpine.store('average')

            }];

            for (let key in Alpine.store('hiders_seekers_distances')) {
                for (let key2 in Alpine.store('hiders_seekers_distances')[key]) {
                    data_chart.push({
                        name: key + " - " + key2,
                        type: 'spline',
                        showInLegend: true,
                        dataPoints: Alpine.store('hiders_seekers_distances')[key][key2]
                    })
                }
            }
            console.log(data_chart);
            Alpine.store("data_chart", data_chart);

            // initialize chart
            chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: false,
                title: {
                    text: "Distances between Hidders and Seekers"
                },
                data: Alpine.store('data_chart')
            });
            chart.render();

            let width = data['width']
            let height = data['height']

            let percentProgress = parseInt(parseFloat(data['round']) / parseFloat(data['maxRounds']) * 100.);

            Alpine.store('gameProgress', percentProgress);

            maze = new ThreeMaze(window, wrapper, width, height);

            // Inits
            maze.initScene();
            maze.onWindowResize();
            maze.render();
            var map = [];
            let counter = 0;

            for (var x = 1; x <= width; x += 1) {
                map[x] = [];

                for (var y = 1; y <= height; y += 1) {
                    if ('block' in data['cells'][counter]) {
                        map[x][y] = data['cells'][counter]["block"]
                    }
                    if ('player' in data['cells'][counter]) {
                        map[x][y] = data['cells'][counter]["player"]
                    }
                    counter++;
                }
            }
            // load maze map
            maze.onGenerateMaze(map);

            window.addEventListener('resize', maze.onWindowResize.bind(maze));
        }).catch(e => {
            console.log(e)
            Alpine.store('gameInfo', "Game not found...");
            Alpine.store('gameError', true);
        });
}

const updateLoop = () => {

    let id = Alpine.store('gameId');

    fetch(`${SERVER_URL}/game/step`, {
            method: 'POST',
            body: JSON.stringify({ "id": id })
        })
        .then(res => {

            // check for error response
            if (!res.ok) {
                // get error message from body or default to response status
                const error = res.status;
                return Promise.reject(error);
            }

            // remove error message (if exists)
            Alpine.store('gameInfo', "");

            return res.json();
        })
        .then(data => {
            // no server error currently
            Alpine.store('gameError', false);
            Alpine.store('gameRound', data["round"]);
            Alpine.store('gameMaxRounds', data["maxRounds"]);
            Alpine.store('gameHider', data["hider"]);
            Alpine.store('gameSeeker', data["seeker"]);

            let width = data['width']
            let height = data['height']
            console.log(data['round'], 'of', data['maxRounds'], '=> end? : ', data['end']);
            let gameEnd = data['end'];

            Alpine.store('gameEnd', gameEnd);

            if (gameEnd) {
                // clearInterval(canvasInterval);
                let currentWinner = data['winnerStr'];
                Alpine.store('gameInfo', `Game ${Alpine.store("gameId")} is finished! Winner is: ${currentWinner}`)
                Alpine.store('gameWinner', currentWinner);
            }

            let percentProgress = parseInt(parseFloat(data['round']) / parseFloat(data['maxRounds']) * 100.);

            Alpine.store('gameProgress', percentProgress);

            var map = [];
            let counter = 0;

            // generate map
            for (var x = 1; x <= width; x += 1) {
                map[x] = [];

                for (var y = 1; y <= height; y += 1) {
                    if ('block' in data['cells'][counter]) {
                        map[x][y] = data['cells'][counter]["block"]
                    }
                    if ('player' in data['cells'][counter]) {
                        map[x][y] = data['cells'][counter]["player"]
                        if (data["currentPlayer"]["name"] === data['cells'][counter]["player"]["name"] && data["currentPlayer"]["roleStr"] === "SEEKER") {
                            for (var i = 1; i < data["currentObservation"]["cells"].length; i++) {

                                // if seeker sees hider on first turn, play animation
                                if (data['round'] === 0) {
                                    if (data["currentObservation"]["cells"][i]["player"] &&
                                        data["currentObservation"]["cells"][i]["player"]["roleStr"] === "HIDER") {

                                        map[x][y]['surpris'] = true
                                    }
                                }

                                // (re)play animation if we observe a hider
                                if (data["currentObservation"]["cells"][i]["player"] && !data["previousObservation"]["cells"][i]["player"] &&
                                    data["currentObservation"]["cells"][i]["player"]["roleStr"] === "HIDER") {

                                    map[x][y]['surpris'] = true
                                }
                            }
                        }
                    }
                    counter++;
                }
            }
            map['currentPlayer'] = data['currentPlayer']
            map['currentAction'] = data['currentAction']
            map['capturedPlayer'] = []
            for (let i = 0; i < data["players"].length; i++) {
                if (data['players'][i]['captured']) {
                    map['capturedPlayer'].push(data['players'][i])
                }
            }
            // reload maze map
            if (maze !== undefined) {
                maze.onGenerateMaze(map);
            }
            if (!data['end']) {
                let found = 0;
                for (let i = 0; i < data["players"].length; i++) {
                    if (data['players'][i]['captured']) {
                        found++;
                    }
                }
                Alpine.store("found", found);

                let players = Alpine.store("players");
                let hider_loacations = [];
                let hider_names = [];
                let seeker_loacations = [];
                let seeker_names = [];


                for (let i = 0; i < players.length; i++) {
                    {
                        // add data for distance calculation
                        if (data['players'][i]['roleStr'] === "SEEKER") {
                            seeker_loacations.push(data['players'][i]['location']);
                            seeker_names.push(data['players'][i]['name']);
                        } else if (data['players'][i]['roleStr'] === "HIDER") {
                            if (!data['players'][i]['captured']) {
                                hider_loacations.push(data['players'][i]['location']);
                                hider_names.push(data['players'][i]['name']);
                            }
                        }
                        // add data for statistics
                        if (players[i]["name"] === data["currentPlayer"]["name"]) {
                            if (data["currentAction"]['interaction'] === 0) {
                                players[i]["steps"] += 1
                            } else if (data["currentAction"]['interaction'] === 2 ||
                                data["currentAction"]['interaction'] === 3 ||
                                data["currentAction"]['interaction'] === 1) {
                                let changedBlocks = Alpine.store("changedBlocks");
                                changedBlocks++;
                                Alpine.store("changedBlocks", changedBlocks);
                            }
                        }
                    }
                }

                Alpine.store("players", players);

                let hiders_seekers_distances = Alpine.store("hiders_seekers_distances");
                let average_distance = 0;

                // calculate distances
                for (let j = 0; j < seeker_loacations.length; j++) {
                    for (let k = 0; k < hider_loacations.length; k++) {
                        let distance = Math.sqrt(Math.pow(hider_loacations[k]['x'] - seeker_loacations[j]['x'], 2) +
                            Math.pow(hider_loacations[k]['y'] - seeker_loacations[j]['y'], 2));
                        average_distance += distance;
                        hiders_seekers_distances[seeker_names[j]][hider_names[k]].push({ y: distance });
                    }
                }
                average_distance /= hider_loacations.length * seeker_loacations.length;
                let average = Alpine.store("average");
                average.push({ y: average_distance });

                // store chart data
                Alpine.store("average", average);
                Alpine.store("hiders_seekers_distances", hiders_seekers_distances);

                let data_chart = [{
                    name: 'Average',
                    type: 'spline',
                    showInLegend: true,
                    dataPoints: Alpine.store('average')

                }];

                for (let key in Alpine.store('hiders_seekers_distances')) {
                    for (let key2 in Alpine.store('hiders_seekers_distances')[key]) {
                        data_chart.push({
                            name: key + " - " + key2,
                            type: 'spline',
                            showInLegend: true,
                            dataPoints: Alpine.store('hiders_seekers_distances')[key][key2]
                        })
                    }
                }
                Alpine.store("data_chart", data_chart);

                chart.render();

            }
        })
        .catch((e) => {
            console.log(e)

            Alpine.store('gameInfo', "Server cannot be reached");
            Alpine.store('gameError', true);
        });
}

export { load, updateLoop };