import * as THREE from 'three';

/**
 * Maze class
 * @param wrapper
 * @param button
 */
class ThreeMaze {
    constructor(window, wrapper, width, height) {
        // Object attributes
        this.window = window
        this.wrapper = wrapper;
        this.camera = {};
        this.scene = {};
        this.materials = {};
        this.map = []
        this.minimap = [];
        this.renderer = {};
        this.width = width;
        this.height = height;
        this.thickness = 30;
        this.minimap_scale = 1 / 30;
        this.scene_minimap = {};
        this.camera_minimap = {};
        this.renderer_minimap = {};
        this.animation = []
    }

    generateEmptyMap = (width, height) => {
        var map = [];

        for (var x = 1; x <= width; x += 1) {
            map[x] = [];
            for (var y = 1; y <= height; y += 1) {
                map[x][y] = {};
            }
        }

        return map;
    }

    onGenerateMaze(map_representation) {
        var new_map = this.generateEmptyMap(this.width, this.height);
        var new_minimap = this.generateEmptyMap(this.width, this.height);

        for (var x = this.width; x > 0; x -= 1) {
            for (var y = 1; y < this.height + 1; y += 1) {
                // store current kind of cell
                new_map[x][y] = {
                    'kind': map_representation[x][y], // block or player
                    'resistance': map_representation[x][y]["resistance"],
                    'surpris': map_representation[x][y]['surpris']
                }


                // check if necessary to update (need to be improved => some blocks can be damaged)
                if (typeof this.map[x] != 'undefined' && typeof this.map[x][y] != 'undefined') {
                    // if it's the same block, we pass
                    if (typeof map_representation[x][y].kind !== 'undefined' && typeof this.map[x][y].kind.kind !== 'undefined') {
                        if (map_representation[x][y]['kind'] === this.map[x][y].kind['kind'] && this.map[x][y].kind['resistance'] === map_representation[x][y]['resistance'] &&
                            !map_representation[x][y].name) {
                            new_map[x][y] = this.map[x][y];
                            new_minimap[x][y] = this.minimap[x][y]
                            continue;
                        }
                    }
                    // check if player and take care of this case
                    if (map_representation[x][y].name !== undefined && !this.map[x][y].captured) {

                        // if a player is captured, we remove it
                        for (let i = 0; i < map_representation['capturedPlayer'].length; i++) {
                            if (map_representation['capturedPlayer'][i].name === this.map[x][y]['kind'].name) {
                                this.map[x][y]['captured'] = true;
                                this.map[x][y].player.visible = false;
                                this.minimap[x][y].player.visible = false;
                                this.scene.remove(this.map[x][y].player);
                                this.scene_minimap.remove(this.minimap[x][y].player);
                            }
                        }


                        // if the player is on the same cell and he changed his orientation, we apply the rotation on the model
                        if (this.map[x][y]["kind"]["orientation"] !== map_representation[x][y]["orientation"] &&
                            map_representation['currentPlayer']['name'] === this.map[x][y]["kind"]['name']) {
                            this.rotate(this.map[x][y]["kind"]["orientation"], map_representation[x][y]["orientation"], this.map[x][y].player)
                                // update the player orientation and check if the player do an action
                            new_map[x][y] = this.map[x][y];
                            new_map[x][y].kind = map_representation[x][y]
                        }
                        // determine the action of the player
                        if (typeof map_representation["currentAction"] !== "undefined" && map_representation['currentPlayer']['name'] === map_representation[x][y].name) {
                            // if the player moved
                            if (map_representation["currentAction"]["interaction"] === 0) {
                                // search from where the player come
                                let position_prev
                                if (map_representation[x][y]['name'] === this.map[x - 1][y]["kind"]['name']) position_prev = { 'x': x - 1, 'y': y }
                                else if (map_representation[x][y]['name'] === this.map[x + 1][y]["kind"]['name']) position_prev = { 'x': x + 1, 'y': y }
                                else if (map_representation[x][y]['name'] === this.map[x][y - 1]["kind"]['name']) position_prev = { 'x': x, 'y': y - 1 }
                                else if ((map_representation[x][y]['name'] === this.map[x][y + 1]["kind"]['name'])) position_prev = { 'x': x, 'y': y + 1 }
                                else {
                                    console.log("can't find the previous position of the player")
                                    console.log(map_representation, this.map)
                                }
                                // play move animation
                                this.move(this.map[position_prev['x']][position_prev['y']]['kind']['orientation'],
                                    position_prev, {
                                        'x': x,
                                        'y': y
                                    }, this.map[position_prev['x']][position_prev['y']].player)

                                // if it's a seeker and he just found a hider, we play the animation
                                if (map_representation[x][y]['roleStr'] === "SEEKER") {
                                    if (new_map[x][y].surpris) {
                                        this.surprise(this.map[position_prev['x']][position_prev['y']].player)
                                    }
                                }

                                // update the player position
                                new_map[x][y] = this.map[position_prev['x']][position_prev['y']];
                                new_map[x][y].kind = map_representation[x][y]


                                // update minimap
                                this.scene_minimap.remove(this.minimap[position_prev['x']][position_prev['y']].player);
                                var circle_geometry = new THREE.CircleGeometry(0.4 * this.thickness * this.minimap_scale, 32);
                                if (map_representation[x][y]['roleStr'] === "SEEKER") {
                                    this.minimap[x][y].player = this.set_minimap_player(new THREE.Mesh(circle_geometry, this.simple_materials.seeker), x, y);
                                } else {
                                    this.minimap[x][y].player = this.set_minimap_player(new THREE.Mesh(circle_geometry, this.simple_materials.hider), x, y);
                                }
                                this.scene_minimap.add(this.minimap[x][y].player);
                                new_minimap[x][y] = this.minimap[x][y]
                                continue

                            } else if (map_representation["currentAction"]["interaction"] === 2) {
                                // generate the block that the player is carrying
                                if (map_representation[x][y]["block"]) {
                                    var mesh;
                                    var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness, this.thickness, 1, 1, 1);
                                    if (map_representation[x][y]["block"]["materialStr"] === "WOOD") {
                                        // generate the mesh for wood
                                        mesh = new THREE.Mesh(wall_geometry, this.materials.wood);
                                    } else if (map_representation[x][y]["block"]["materialStr"] === "STONE") {
                                        // generate the mesh for stone
                                        if (map_representation[x][y]["block"]["resistance"] === 100) mesh = new THREE.Mesh(wall_geometry, this.materials.stone);
                                        else mesh = new THREE.Mesh(wall_geometry, this.materials.damaged1_stone);
                                    } else {
                                        // generate the mesh for metal
                                        if (map_representation[x][y]["block"]["resistance"] === 100) mesh = new THREE.Mesh(wall_geometry, this.materials.metal);
                                        else if (map_representation[x][y]["block"]["resistance"] === 80) mesh = new THREE.Mesh(wall_geometry, this.materials.damaged_1_metal);
                                        else if (map_representation[x][y]["block"]["resistance"] === 60) mesh = new THREE.Mesh(wall_geometry, this.materials.damaged_2_metal);
                                        else if (map_representation[x][y]["block"]["resistance"] === 40) mesh = new THREE.Mesh(wall_geometry, this.materials.damaged_3_metal);
                                        else mesh = new THREE.Mesh(wall_geometry, this.materials.damaged_4_metal);
                                    }
                                    mesh.scale.set(.4, .4 * map_representation[x][y]["block"]["resistance"] / 100, .4)
                                    mesh.position.set(0, this.thickness, 0);
                                    this.map[x][y].player.mesh = mesh
                                    this.map[x][y].player.add(mesh)
                                }
                            } else if (map_representation["currentAction"]["interaction"] === 3) {
                                // remove the block from the player
                                this.map[x][y].player.mesh.visible = false;
                                this.scene.remove(this.map[x][y].player.mesh)
                            }

                            // if it's a seeker and he just found a hider, we play the animation
                            if (map_representation[x][y]['roleStr'] === "SEEKER") {
                                if (new_map[x][y].surpris) {
                                    this.surprise(this.map[x][y].player)
                                }
                            }

                            new_map[x][y] = this.map[x][y]
                            new_minimap[x][y] = this.minimap[x][y]
                            continue
                        } else {

                            // if it's a seeker and he just found a hider, we play the animation
                            if (map_representation[x][y]['roleStr'] === "SEEKER") {
                                if (new_map[x][y].surpris) {
                                    this.surprise(this.map[x][y].player)
                                }
                            }
                            new_map[x][y] = this.map[x][y]
                            new_minimap[x][y] = this.minimap[x][y]
                            continue
                        }
                    } else {
                        this.map[x][y].mesh.visible = false;
                        this.scene.remove(this.map[x][y].mesh);
                        this.scene_minimap.remove(this.minimap[x][y].mesh);
                    }
                }

                var plane_geometry = new THREE.PlaneGeometry(this.thickness * this.minimap_scale,
                    this.thickness * this.minimap_scale);
                var circle_geometry = new THREE.CircleGeometry(0.4 * this.thickness * this.minimap_scale, 32);
                // Adds a new mesh if needed
                if (map_representation[x][y]["kindStr"] === "BORDER") {
                    // Generates the mesh for border
                    var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2,
                        this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(wall_geometry, this.materials.grey);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2),
                        0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                    // add border to minimap
                    new_minimap[x][y].mesh = this.set_minimap_mesh(new THREE.Mesh(plane_geometry, this.simple_materials.grey), x, y);
                    this.scene_minimap.add(new_minimap[x][y].mesh);

                } else if (map_representation[x][y]["kindStr"] === "GROUND") {
                    // generate the mesh with ground
                    var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                    // add ground to minimap
                    new_minimap[x][y].mesh = this.set_minimap_mesh(new THREE.Mesh(plane_geometry, this.simple_materials.ground), x, y);
                    this.scene_minimap.add(new_minimap[x][y].mesh);


                } else if (map_representation[x][y]["kindStr"] === "MATERIAL") {
                    var mesh;
                    var simple_mesh;
                    var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2, this.thickness, 1, 1, 1);
                    if (map_representation[x][y]["materialStr"] === "WOOD") {
                        // generate the mesh for wood
                        mesh = new THREE.Mesh(wall_geometry, this.materials.wood);
                        simple_mesh = new THREE.Mesh(plane_geometry, this.simple_materials.wood);
                    } else if (map_representation[x][y]["materialStr"] === "STONE") {
                        // generate the mesh for stone
                        if (map_representation[x][y]["resistance"] === 100) {
                            mesh = new THREE.Mesh(wall_geometry, this.materials.stone);
                            simple_mesh = new THREE.Mesh(plane_geometry, this.simple_materials.stone);
                        } else {
                            mesh = new THREE.Mesh(wall_geometry, this.materials.damaged1_stone);
                            simple_mesh = new THREE.Mesh(plane_geometry, this.simple_materials.damaged1_stone);
                        }
                    } else {
                        // generate the mesh for metal
                        if (map_representation[x][y]["resistance"] === 100) {
                            mesh = new THREE.Mesh(wall_geometry, this.materials.metal);
                            simple_mesh = new THREE.Mesh(plane_geometry, this.simple_materials.metal);
                        } else if (map_representation[x][y]["resistance"] === 80) {
                            mesh = new THREE.Mesh(wall_geometry, this.materials.damaged_1_metal);
                            simple_mesh = new THREE.Mesh(plane_geometry, this.simple_materials.damaged1_metal);
                        } else if (map_representation[x][y]["resistance"] === 60) {
                            mesh = new THREE.Mesh(wall_geometry, this.materials.damaged_2_metal);
                            simple_mesh = new THREE.Mesh(plane_geometry, this.simple_materials.damaged2_metal);
                        } else if (map_representation[x][y]["resistance"] === 40) {
                            mesh = new THREE.Mesh(wall_geometry, this.materials.damaged_3_metal);
                            simple_mesh = new THREE.Mesh(plane_geometry, this.simple_materials.damaged_3_metal);
                        } else {
                            mesh = new THREE.Mesh(wall_geometry, this.materials.damaged_4_metal);
                            simple_mesh = new THREE.Mesh(plane_geometry, this.simple_materials.damaged_4_metal);
                        }
                    }
                    // update block size
                    mesh.scale.set(1, map_representation[x][y]["resistance"] / 100, 1)
                    mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    new_map[x][y].mesh = mesh
                    this.scene.add(new_map[x][y].mesh);
                    // add block to minimap
                    new_minimap[x][y].mesh = this.set_minimap_mesh(simple_mesh, x, y);
                    this.scene_minimap.add(new_minimap[x][y].mesh);

                } else if (map_representation[x][y]["roleStr"] === "SEEKER" && typeof this.map[x] === 'undefined') {
                    // Add seeker
                    var direction = map_representation[x][y]["orientationStr"]
                    new_map[x][y].player = this.character(this.materials.seeker, this.thickness)

                    // apply rotation on the group according to the direction of the player
                    if (direction === "Top") new_map[x][y].player.rotateY(THREE.Math.degToRad(-90));
                    else if (direction === "Bottom") new_map[x][y].player.rotateY(THREE.Math.degToRad(90));
                    else if (direction === "Right") new_map[x][y].player.rotateY(THREE.Math.degToRad(0));
                    else new_map[x][y].player.rotateY(THREE.Math.degToRad(180));

                    new_map[x][y].player.visible = true
                    new_map[x][y].player.position.set(x * this.thickness - ((this.width * this.thickness) / 2), this.thickness, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].player);
                    // add the player to the minimap
                    new_minimap[x][y].player = this.set_minimap_player(new THREE.Mesh(circle_geometry, this.simple_materials.seeker), x, y);
                    this.scene_minimap.add(new_minimap[x][y].player);
                    if (new_map[x][y].surpris) {
                        this.surprise(new_map[x][y].player)
                    }

                    // also add the ground mesh
                    var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1,
                        1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2),
                        0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                    // add the ground to the minimap
                    new_minimap[x][y].mesh = this.set_minimap_mesh(new THREE.Mesh(plane_geometry, this.simple_materials.ground), x, y);

                    this.scene_minimap.add(new_minimap[x][y].mesh);
                } else if (typeof this.map[x] === 'undefined') {
                    // Add hider
                    var direction = map_representation[x][y]["orientationStr"];
                    new_map[x][y].player = this.character(this.materials.hider, this.thickness);
                    // apply rotation on the group according to the direction of the player
                    if (direction === "Top") new_map[x][y].player.rotateY(THREE.Math.degToRad(-90));
                    else if (direction === "Bottom") new_map[x][y].player.rotateY(THREE.Math.degToRad(90));
                    else if (direction === "Right") new_map[x][y].player.rotateY(THREE.Math.degToRad(0));
                    else new_map[x][y].player.rotateY(THREE.Math.degToRad(180));
                    new_map[x][y].player.visible = true
                    new_map[x][y].player.position.set(x * this.thickness - ((this.width * this.thickness) / 2), this.thickness, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].player);
                    // add the player to the minimap
                    new_minimap[x][y].player = this.set_minimap_player(new THREE.Mesh(circle_geometry, this.simple_materials.hider), x, y);
                    this.scene_minimap.add(new_minimap[x][y].player);

                    // also add the ground mesh
                    var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                    // add the ground to the minimap
                    new_minimap[x][y].mesh = this.set_minimap_mesh(new THREE.Mesh(plane_geometry, this.simple_materials.ground), x, y);
                    this.scene_minimap.add(new_minimap[x][y].mesh);
                }
            }
        }


        this.map_representation = map_representation;
        this.map = new_map;
        this.minimap = new_minimap;
    };

    // set mesh position on the minimap
    set_minimap_mesh(mesh, x, y) {
        mesh.position.y = -this.minimap_scale * (x * this.thickness - ((this.width * this.thickness) / 2));
        mesh.position.x = -this.minimap_scale * (y * this.thickness - ((this.height * this.thickness) / 2));
        mesh.position.z = 0;
        return mesh;
    }

    // set player position on the minimap
    set_minimap_player(player, x, y) {
        player.position.y = -this.minimap_scale * (x * this.thickness - ((this.width * this.thickness) / 2));
        player.position.x = -this.minimap_scale * (y * this.thickness - ((this.height * this.thickness) / 2));
        player.position.z = 0.1;
        return player;
    }

    // create a character
    character(material, thickness) {
        let amogus = new THREE.Group()
        let cylinder_geometry = new THREE.CylinderGeometry(thickness / 3, thickness / 3, thickness / 2, 32)

        // create a pill shape for the character base 
        function pill(base, material) {
            let pill = new THREE.Group()
            let cylinder = new THREE.Mesh(base, material)
            pill.add(cylinder)
            for (let i = 0; i < 2; i++) {
                let sphere_geometry = new THREE.SphereGeometry(thickness / 3, 16, 16)
                let sphere = new THREE.Mesh(sphere_geometry, material)
                const m = i % 2 === 0 ? 1 : -1
                sphere.position.y = m * base.parameters.height / 2
                pill.add(sphere)
            }
            return pill
        }

        // create a body for the character
        let pill_body = pill(cylinder_geometry, material)
        amogus.add(pill_body)

        // create feet
        for (let i = 0; i < 2; i++) {
            let pill_foot = pill(cylinder_geometry, material)
            const m = i % 2 === 0 ? 1 : -1
            pill_foot.position.y = -cylinder_geometry.parameters.height
            pill_foot.position.x = m * cylinder_geometry.parameters.radiusBottom / 2
            pill_foot.scale.set(0.35, 0.4, 0.35)
            amogus.add(pill_foot)
        }


        // create back for the character
        let pill_back = pill(cylinder_geometry, material)
        pill_back.scale.set(0.55, 0.6, 0.55)
        pill_back.position.z = -cylinder_geometry.parameters.radiusBottom / 1.4
        amogus.add(pill_back)


        // create glasses for the characters
        let glasses_material = new THREE.MeshBasicMaterial({ color: 0xFFFFF0 })
        let glasses = pill(cylinder_geometry, glasses_material)
        glasses.scale.set(0.45, 0.4, 0.4)
        glasses.rotateZ(Math.PI / 2)
        glasses.position.z = cylinder_geometry.parameters.radiusBottom / 1.3
        glasses.position.y = cylinder_geometry.parameters.radiusBottom * 0.5
        amogus.add(glasses)

        return amogus
    }

    // create exclamation mark
    exclam(thickness) {
        let exclamation_mark = new THREE.Group()

        let exclamation_square = new THREE.BoxGeometry(thickness / 10, thickness / 10, thickness / 10, 1, 1, 1);
        let mesh = new THREE.Mesh(exclamation_square, new THREE.MeshBasicMaterial({ color: 0xFF5500 }))
        mesh.position.y = -(thickness / 3)
        exclamation_mark.add(mesh)

        let exclamation_rectangle = new THREE.BoxGeometry(thickness / 10, thickness / 3, thickness / 10, 1, 1, 1);
        mesh = new THREE.Mesh(exclamation_rectangle, new THREE.MeshBasicMaterial({ color: 0xFF5500 }))
        exclamation_mark.add(mesh)
        exclamation_mark.position.y = 1.2 * thickness

        return exclamation_mark
    }


    // create a group of 3 '!' for surprise animation
    get_exclam(thickness) {
        let exclam_group = new THREE.Group()
        exclam_group.add(this.exclam(thickness))
        for (let i = 0; i < 2; i++) {
            let side = this.exclam(thickness)
            const m = i % 2 === 0 ? 1 : -1
            side.position.x = m * thickness / 2.5
            side.position.y -= thickness / 15
            side.rotation.z = m * -0.35
            exclam_group.add(side)
        }
        return exclam_group
    }

    /**
     * Inits the scene
     */
    initScene() {
        // Scenes
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0xffffff);
        this.scene_minimap = new THREE.Scene();
        this.scene_minimap.background = new THREE.Color(0xffffff);

        // Materials
        const loader = new THREE.TextureLoader();

        const ground_material = new THREE.MeshBasicMaterial({
            map: loader.load('https://tse2.mm.bing.net/th?id=OIP.gnc6dyhXu6CUF_lBJ5b7KwHaHa&pid=Api'),
        })

        const wall_material = new THREE.MeshBasicMaterial({
            map: loader.load('https://tse2.mm.bing.net/th?id=OIP.rgCGU0PU4_fcq6sVeGJyZgHaE8&pid=Api'),
            wireframe: false
        })

        const wood_material = new THREE.MeshBasicMaterial({
            map: loader.load('Image/wood.jpeg'),
            wireframe: false
        })

        const stone_material = new THREE.MeshBasicMaterial({
            map: loader.load('Image/stone.jpg'),
            wireframe: false
        })

        const damaged1_stone_material = new THREE.MeshBasicMaterial({
            map: loader.load('Image/stone2.jpeg'),
            wireframe: false
        })

        const metal_material = new THREE.MeshBasicMaterial({
            map: loader.load('Image/metal.jpeg'),
            wireframe: false
        })

        const damaged1_metal_material = new THREE.MeshBasicMaterial({
            map: loader.load('Image/metal2.jpg'),
            wireframe: false
        })

        const damaged2_metal_material = new THREE.MeshBasicMaterial({
            map: loader.load('Image/metal3.jpg'),
            wireframe: false
        })

        const damaged3_metal_material = new THREE.MeshBasicMaterial({
            map: loader.load('Image/metal4.jpg'),
            wireframe: false
        })

        const damaged4_metal_material = new THREE.MeshBasicMaterial({
            map: loader.load('Image/metal5.jpg'),
            wireframe: false
        })

        // materials for the labyrinth
        this.materials = {
            grey: wall_material,
            wood: wood_material,
            stone: stone_material,
            damaged1_stone: damaged1_stone_material,
            metal: metal_material,
            damaged_1_metal: damaged1_metal_material,
            damaged_2_metal: damaged2_metal_material,
            damaged_3_metal: damaged3_metal_material,
            damaged_4_metal: damaged4_metal_material,
            ground: ground_material,
            hider: new THREE.MeshLambertMaterial({ color: 0x0066ff }),
            seeker: new THREE.MeshLambertMaterial({ color: 0x990000 }),
            orange: new THREE.MeshLambertMaterial({ color: 0xbe842b }),
        };

        // materials for the minimap
        this.simple_materials = {
            grey: new THREE.MeshBasicMaterial({ color: 0x000000 }),
            wood: new THREE.MeshBasicMaterial({ color: 0x663300 }),
            stone: new THREE.MeshBasicMaterial({ color: 0xB5D2DA }),
            damaged1_stone: new THREE.MeshBasicMaterial({ color: 0x99B6BD }),
            metal: new THREE.MeshBasicMaterial({ color: 0xE0E0E0 }),
            damaged_1_metal: new THREE.MeshBasicMaterial({ color: 0xC0C0C0 }),
            damaged_2_metal: new THREE.MeshBasicMaterial({ color: 0xA0A0A0 }),
            damaged_3_metal: new THREE.MeshBasicMaterial({ color: 0x808080 }),
            damaged_4_metal: new THREE.MeshBasicMaterial({ color: 0x606060 }),
            ground: new THREE.MeshBasicMaterial({ color: 0xC9B828 }),
            hider: new THREE.MeshBasicMaterial({ color: 0x0000ff }),
            seeker: new THREE.MeshBasicMaterial({ color: 0xff0000 }),
        };

        // Camera for labyrinth
        this.camera = new THREE.PerspectiveCamera(45, 1, 1, 2000);
        this.camera.position.x = 1000;
        this.camera.position.y = 800;
        this.camera.position.z = -300;
        this.camera.clicked = false;

        // Camera for minimap
        this.camera_minimap = new THREE.PerspectiveCamera(45, 1, 1, 2000);
        this.camera_minimap.position.z = 30;
        this.camera_minimap.position.y = 0;
        this.camera_minimap.position.x = 0;
        this.camera_minimap.clicked = false;

        // Lights
        this.scene.add(new THREE.AmbientLight(0xc9c9c9));
        var directional = new THREE.DirectionalLight(0xc9c9c9, 0.5);
        directional.position.set(0, -150, 0);
        this.scene.add(directional);

        // Renderers
        this.renderer = typeof WebGLRenderingContext != 'undefined' && window.WebGLRenderingContext ? new THREE.WebGLRenderer({ antialias: true }) : new THREE.CanvasRenderer({});
        this.renderer_minimap = typeof WebGLRenderingContext != 'undefined' && window.WebGLRenderingContext ? new THREE.WebGLRenderer({ antialias: true }) : new THREE.CanvasRenderer({});
        this.wrapper.appendChild(this.renderer.domElement);
        this.renderer_minimap.domElement.style.position = 'absolute';
        this.renderer_minimap.domElement.style.top = '120px';
        this.renderer_minimap.domElement.style.right = '10px';
        this.wrapper.appendChild(this.renderer_minimap.domElement);
    };

    // surprise animation
    surprise(character) {
        let exclam = this.get_exclam(this.thickness)
        character.add(exclam)
        let info = []
        info['effect'] = exclam;
        info['direction'] = 0.2 * this.thickness;
        info['character'] = character
        info['type'] = "surprise"
        info['destination'] = 2 * this.thickness
        this.animation[this.animation.length] = info;
    }

    // rotation animation
    rotate(origin, destination, character, next_animation) {
        if (destination === origin) {
            if (next_animation) this.animation[this.animation.length] = next_animation
        } else {
            let info = {}
            let angle
                // determine the rotation angle
            if ((origin < 2 && destination < 2) || (origin >= 2 && destination >= 2)) {
                angle = 180;
                info['direction'] = 18
                info['destination'] = angle
            } else if ((origin === 0 && destination === 3) || (origin === 1 && destination === 2) ||
                (origin === 2 && destination === 0) || (origin === 3 && destination === 1)) {
                angle = 90;
                info['direction'] = 18
                info['destination'] = angle
            } else {
                angle = 90;
                info['direction'] = -18
                info['destination'] = angle
            }
            info['character'] = character
            info['type'] = "rotation"

            info['next_animation'] = next_animation
            this.animation[this.animation.length] = info
        }
    }

    // move animation
    move(current_orientation, origin, dest, character) {
        let movement_infos = {};
        movement_infos['type'] = 'movement';
        movement_infos['character'] = character;
        movement_infos['direction'] = 0.1 * this.thickness;
        // determine the movement axis
        if (origin['y'] === dest['y']) {
            movement_infos['destination'] = (dest['x'] - origin['x']) * this.thickness
            movement_infos['axis'] = "x";
            // chain animation with rotation
            if (movement_infos['destination'] >= 0) this.rotate(current_orientation, 1, character, movement_infos);
            else this.rotate(current_orientation, 0, character, movement_infos);
        } else {
            movement_infos['destination'] = (dest['y'] - origin['y']) * this.thickness;
            movement_infos['axis'] = "z";
            // chain animation with rotation
            if (movement_infos['destination'] >= 0) this.rotate(current_orientation, 3, character, movement_infos);
            else this.rotate(current_orientation, 2, character, movement_infos);
        }
    }


    update() {
        for (let i = 0; i < this.animation.length; i++) {
            // if animation is a surprise
            if (this.animation[i]['type'] === "surprise") {
                this.animation[i]['character'].position.y += this.animation[i]['direction']
                if (this.animation[i]['character'].position.y >= this.animation[i]['destination']) this.animation[i]['direction'] *= -1
                if (this.animation[i]['character'].position.y <= this.thickness) {
                    this.animation[i]['character'].remove(this.animation[i]['effect'])
                    this.animation.splice(i, 1)
                }
                // if animation is a rotation
            } else if (this.animation[i]['type'] === "rotation") {
                this.animation[i]['character'].rotateY(Math.PI / 180 * (this.animation[i]["direction"]))

                // positive rotation
                if (this.animation[i]['direction'] >= 0) {
                    this.animation[i]['destination'] -= this.animation[i]['direction']
                    if (this.animation[i]['destination'] <= 0) {
                        // if there is a next animation, go to next animation
                        if (this.animation[i]['next_animation']) {
                            this.animation[i] = this.animation[i]['next_animation']
                        } else {
                            this.animation.splice(i, 1)
                        }
                    }
                    // negative rotation
                } else {
                    this.animation[i]['destination'] += this.animation[i]['direction']
                    if (this.animation[i]['destination'] <= 0) {
                        // if there is a next animation, go to next animation
                        if (this.animation[i]['next_animation']) {
                            this.animation[i] = this.animation[i]['next_animation']
                        } else {
                            this.animation.splice(i, 1)
                        }
                    }
                }
                // if animation is a movement
            } else if (this.animation[i]['type'] === 'movement') {
                // negative movement
                if (this.animation[i]['destination'] < 0) {
                    if (this.animation[i]["axis"] === "z") this.animation[i]['character'].position.z -= this.animation[i]['direction'];
                    else this.animation[i]['character'].position.x -= this.animation[i]['direction'];
                    this.animation[i]['destination'] += this.animation[i]['direction'];
                    // positive movement
                } else {
                    if (this.animation[i]["axis"] === "z") this.animation[i]['character'].position.z += this.animation[i]['direction'];
                    else this.animation[i]['character'].position.x += this.animation[i]['direction'];
                    this.animation[i]['destination'] -= this.animation[i]['direction'];
                }
                // remove animation if it is finished
                if (this.animation[i]['destination'] == 0) {
                    this.animation.splice(i, 1)
                }
            }
        }
    }

    /**
     * Render loop
     * Sets the camera position and renders the scene
     */
    render() {
        requestAnimationFrame(this.render.bind(this));
        this.update()
        this.camera.lookAt(new THREE.Vector3(0, -150, 0));
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.render(this.scene, this.camera);
        this.renderer_minimap.setPixelRatio(window.devicePixelRatio);
        this.renderer_minimap.render(this.scene_minimap, this.camera_minimap);

    };


    /**
     * Sets the scene dimensions on window resize
     */
    onWindowResize() {
        // Check if there is a better way to adjust width and height
        var width = (this.window.innerWidth / 12) * 8.5 || this.window.document.body.clientWidth;
        var height = this.window.innerHeight - 120 || this.window.document.body.clientHeight;
        this.renderer.setSize(width, height);
        this.renderer_minimap.setSize(width * 0.2, width * 0.2);
        this.camera.aspect = width / height;
        this.camera_minimap.aspect = 1;
        this.camera.updateProjectionMatrix();
        this.camera_minimap.updateProjectionMatrix();
    };
}

export default ThreeMaze;